import * as d3 from "d3"
import { Shooting } from "./shooting"
import { Util } from "./util"
import { SelectedIds } from "./app";
import { stackOrderReverse } from "d3";

interface IDataCountItem {
  date: Date,
  A: number,
  H: number,
  W: number,
  B: number,
  O: number,
  N: number
}

class DataCountItem implements IDataCountItem {
    public date: Date; 
    public A: number;
    public H: number; 
    public W: number;
    public B: number;
    public O: number;
    public N: number;

    public total() { return (this.A || 0) + (this.H || 0) + (this.W  || 0) + (this.B || 0) + (this.O || 0) + (this.N || 0); }
}

interface StackedDataItem {
  0: number,
  1: number,
  data: IDataCountItem
}

export class StackedLineChart {
  raceColorMap: {[key: string]: string} = {"A":"#a6cee3", "H":"#1f78b4", "W":"#b2df8a", "B":"#33a02c", "O":"#fb9a99", "N":"#e31a1c", "":"#fdbf6f"}; // http://colorbrewer2.org/#type=qualitative&scheme=Paired&n=7
  util = new Util();

  constructor(public data: Shooting[]) {
    /* const keys = d3
      .map(this.data, d => d.race)
      .keys(); */

    // render y axis label
    this.g.append("text")
      .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
      .attr("transform", `translate(${this.plotMargins.left / -2},${this.height / 2})rotate(-90)`)  // text is drawn off the PLOT (!) top left, move down and left and rotate
      .attr("font-family", "sans-serif")
      .text("People shot");

    this.tooltip.append("rect")
      .attr("width", 140)
      .attr("fill", "white")
      .style("opacity", 0.85)
    
    this.tooltip.append("text")
      .attr("x", 15)
      .attr("dy", "1.2em")
      .attr("font-family", "sans-serif")
      .attr("font-size", "12px")
      .attr("font-weight", "bold");
  }
  
  readonly svgWidth = 800;
  readonly svgHeight = 350;
  readonly plotMargins = { top: 20, bottom: 60, left: 60, right: 20 };
  readonly width = this.svgWidth - this.plotMargins.left - this.plotMargins.right;
  readonly height = this.svgHeight - this.plotMargins.top - this.plotMargins.bottom;
  
  // Creates sources <svg> element
  readonly svg = d3.select(".stackedLineChart").append('svg')
    .attr('width', this.svgWidth)
    .attr('height', this.svgHeight);
  
  // Group used to enforce margin
  readonly g = this.svg.append('g')
    .classed('plot', true)
    .attr('transform', `translate(${this.plotMargins.left},${this.plotMargins.top})`);

    // Prep the tooltip, initial display is hidden
  readonly tooltip = this.svg.append("g")
    .attr("class", "mousetooltip") // do not name it tooltip, otherwise bootstrap overrides css
    .style("display", "none");

  readonly monthLine = this.svg.append("line")
    .attr("class", "monthline")
    .attr("stroke-width", 1)
    .attr("stroke", "#111")
    .style("display", "none");

  // define scales
  readonly x = d3
    .scaleTime()
    .range([0, this.width]);

  readonly y = d3
    .scaleLinear()
    .range([this.height, 0]);

  readonly z = d3
    .scaleOrdinal(d3.schemeCategory10);

  // define axis
  readonly xaxis = d3.axisBottom(this.x)
    .ticks(d3.timeMonth)
    .tickFormat(d3.timeFormat("%Y-%m"));
    
  readonly g_xaxis = this.g.append('g')
    .attr('class','x axis')
    .attr("transform", "translate(0," + this.height + ")");

  readonly yaxis = d3.axisLeft(this.y);
  readonly g_yaxis = this.g.append('g')
    .attr('class','y axis');

  transformData(data: Shooting[], keys: string[]) { 

    const nestedData = d3.nest<Shooting, number>()
      .key(d => { var parts = d.date.split('-'); return parts[1] + "-" + parts[0]; })
      .entries(data);
    
    const groupedData = nestedData.map(d => ({
        key: d.key,
        values: keys.map(k => { 
                  const count: number = d.values.filter(v => v.race === k).length;
                  return ({key: k, value: count});
            })
    }));

    var parseDate = d3.timeParse("%m-%Y");

    const flattenedData = groupedData.map<DataCountItem>(d => {
        let obj = new DataCountItem(); 
        obj.date = parseDate(d.key);

        d.values.forEach(v => {
            obj[v.key] = v.value;
        });

        return obj;
    });

    return flattenedData;
  }

  getDataPointForMonth(d: d3.Series<DataCountItem, string>, month: Date) {
    for(let i = 0; i < d.length; i++) {
      const item = d[i];
      // we grouped the data to monthly batches
      if (item.data.date.getFullYear() === month.getFullYear() &&
          item.data.date.getMonth() === month.getMonth()) {
        return item;
      }
    }
  }

  update(d: Shooting[], selectedIds: SelectedIds) {  

    const data = this.transformData(d, selectedIds.selectedRaces);
    // set domain according to data
    this.x.domain(d3.extent(data, d => d.date));
    const maxY = d3.max(data, d => d.total());
    this.y.domain([0, maxY])
    this.z.domain(selectedIds.selectedRaces);

    const stack = d3
      .stack<any, DataCountItem, string>().order(stackOrderReverse)
      .keys(selectedIds.selectedRaces);

    const area = d3
      .area<StackedDataItem>()
      .x((d, i) => { return this.x(d.data.date); })
      .y0((d) => { return this.y(d[0]); })
      .y1((d) => { return this.y(d[1]); })
      // .curve(d3.curveBasis); // interpolate, see: http://bl.ocks.org/emmasaunders/raw/c25a147970def2b02d8c7c2719dc7502/

    const stackedData = stack(data);
    // DATA JOIN
    const layer = this.g.selectAll(".layer")
      .data(stackedData);
      
    // rotate x axis labels
    this.g_xaxis.call(this.xaxis)
      .selectAll("text")	
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", ".15em")
      .attr("transform", "rotate(-45)");

    this.g_yaxis.call(this.yaxis);

    // ENTER
    const layer_enter = layer.enter()
      .append("g")
      .attr("class", "layer")
      // Tooltip:
      .on("mouseover", () => { 
        this.tooltip.style("display", null);
        this.monthLine.style("display", null);
      })
      .on("mouseout", () => {
        this.tooltip.style("display", "none");
        this.monthLine.style("display", "none")
       })
      .on("mousemove", (d, i, nodes) => {
        const pos = d3.mouse(this.svg.node() as d3.ContainerElement);
        const xPos = pos[0];
        const yPos = pos[1];

        const hoveredLayer = nodes[i];
        const layerPos = d3.mouse(hoveredLayer as d3.ContainerElement);
        const layerPosX = layerPos[0];
        const layerPosY = layerPos[1];

        // otherwise the mouse is over the vertical line and no layer.mouseover is triggered
        const smoothPadding = 5; 
        const date = this.x.invert(layerPosX + smoothPadding);

        let month: Date;
        
        if (layerPosX <= (this.x(d[1].data.date) + 0.001) / 2) {
          // corner case, when the cursor is within the first half 
          // of the first month-interval, the month "on the left of the curser"
          // should be selected
          month = new Date(date.getFullYear(), date.getMonth());  
        } else {
          const nextDate = new Date(date.getFullYear(), date.getMonth() + 1);

          if (nextDate > this.x.domain()[1]) {
            // don't exceed upper boundary due to smoothPadding
            month = this.x.domain()[1];
          } else {
            month = new Date(date.getFullYear(), date.getMonth() + 1);
          }          
        }

        const item = this.getDataPointForMonth(d, month);

        this.monthLine
          .attr("x1", this.x(month) + this.plotMargins.left).attr("y1", this.y(0) + this.plotMargins.top)
          .attr("x2", this.x(month) + this.plotMargins.left).attr("y2", this.plotMargins.top)

        // put tooltip on left or right side of selected vertical line
        const maxX = this.x(this.x.domain()[1]);
        if (this.x(month) < maxX / 2) {
          this.tooltip.attr("transform", "translate(" + (this.x(month) + 60) + "," + (yPos - 140) + ")");
        } else {
          this.tooltip.attr("transform", "translate(" + (this.x(month) - 80) + "," + (yPos - 140) + ")");
        }

        this.tooltip.select("rect").attr("height", this.z.domain().length * 17 + 10).attr("y", -5); // 10 & -5 for padding

        this.tooltip.selectAll(".tooltip-color")
          .style("fill", (d: string) => this.raceColorMap[d]);

        this.tooltip.selectAll(".tooltip-labeltext")
          .text((d: string) => `${this.util.raceNamesMap[d]}: ${item.data[d]}`);
      })

    const labels = this.tooltip.selectAll(".tooltip-label")
      // using toString() because otherwise it seems d3 takes the index position identify
      .data(this.z.domain(), d => d.toString()); 

    const labels_enter = labels.enter()
      .append("text")
      .attr("class", d => "tooltip-label " + d)
      // .attr("id", d => d)
      .attr("fill", "#111")
      .attr("x", 10)
      .attr("dy", "1.2em")
      .attr("font-family", "sans-serif")
      .attr("font-size", "11px")
      .attr("font-weight", "bold")
      
    labels_enter.append("tspan")
      .attr("class", "tooltip-color")
      .text("⬤ ");

    labels_enter.append("tspan")
      .attr("class", "tooltip-labeltext");


    labels.merge(labels_enter)
      .attr("y", d => this.z.domain().indexOf(d) * 17);

    labels.exit().remove();

    layer_enter.append("path")

    layer.merge(layer_enter).transition()
      .style("fill", d => this.raceColorMap[d.key])
      .select('path').attr("d", area)
      
    layer.exit().remove();
  }
}

