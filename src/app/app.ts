import * as d3 from "d3"
import { GroupedBarChart } from "./groupedBarChart"
import { StackedLineChart } from "./stackedLineChart"
import { Shooting } from "./shooting";
import { Util } from "./util";

document.addEventListener('DOMContentLoaded', function () {
  console.log("Hello from DOMContentLoaded!")

  d3.csv('assets/police-shootings.csv', (csv: Shooting[]) => {
    console.log("Loaded CSV...")
    const data: Shooting[] = csv;
    const lineChart = new StackedLineChart(data);    
    const barChart = new GroupedBarChart(data);
    const main = new Main(lineChart, barChart, data);
  });
});

class Main {
  util;
  nestedData;

  constructor (public lineChart: StackedLineChart, public barChart: GroupedBarChart, public data: Shooting[]) {
    this.util = new Util();
    this.populateCheckboxes();
    this.data = data;
  }

  populateCheckboxes() {
    console.log("#populateCheckboxes")

    d3.select('.race-selection')
    .selectAll('.race-checkbox')
    .data(this.util.races)
    .enter()
    .append('div')
    .attr('class', 'race-checkbox')
    .classed("form-check", true)
    .append("label")
    .html((race: any) => {
      const checkbox: string = '<input id="' + race + '" type="checkbox" checked class="form-check-input">';
      return checkbox + " " + this.util.raceNamesMap[race];
    });

    d3.select(".age-selection")
      .selectAll(".age-checkbox")
      .data(this.util.ageIds)
      .enter()
      .append("div")
      .classed("age-checkbox", true)
      .classed("form-check", true)
      .append("label")
      .html((id, index) => {
        var checkbox: string = '<input id="' + id + '" type="checkbox" checked class="age form-check-input">';
        return checkbox + " " + this.util.ageNames[index];
      });
  
    this.lineChart.update(this.filterData(this.getSelectedIds()), this.getSelectedIds());
    this.barChart.update(this.getNestedData(this.filterData(this.getSelectedIds())), this.getSelectedIds());

    d3.selectAll(".race-checkbox").on('change', () => {
      this.updateBothCharts(this.getSelectedIds());
    });

    d3.selectAll(".age-checkbox").on('change', () => {
      this.updateBothCharts(this.getSelectedIds());
    });
  }

  updateBothCharts(selectedIds: SelectedIds) {
    this.lineChart.update(this.filterData(selectedIds), selectedIds);
    this.barChart.update(this.getNestedData(this.filterData(selectedIds)), selectedIds);
  }

  filterData(selectedIds: SelectedIds) {
    var filteredData: Shooting[] = this.data.filter((d) => {return selectedIds.selectedRaces.indexOf(d.race) > -1;})
    filteredData = filteredData.filter((d) => {return d.age != ""})
    filteredData = filteredData.filter((d) => {
      const age = parseInt(d.age);
      if (age <= 20 && selectedIds.selectedAges.indexOf("<20") == -1) {
        return false;
      } else if (age > 20 && age <= 30 && selectedIds.selectedAges.indexOf("20-30") == -1){
        return false;
      } else if (age > 30 && age <= 40 && selectedIds.selectedAges.indexOf("30-40") == -1){
        return false;
      } else if (age > 40 && age <= 50 && selectedIds.selectedAges.indexOf("40-50") == -1){
        return false;
      } else if (age > 50 && age <= 60 && selectedIds.selectedAges.indexOf("50-60") == -1){
        return false;
      } else if (age > 60 && selectedIds.selectedAges.indexOf(">60") == -1){
        return false;
      }
      return true;
    })
    return filteredData;
  }

  getSelectedIds(): SelectedIds {
    let selectedIds = new SelectedIds;

    d3.selectAll(".race-checkbox input:checked").each(function() {
      selectedIds.selectedRaces.push(d3.select(this).property("id"));
    });

    d3.selectAll(".age-checkbox input:checked").each(function() {
      selectedIds.selectedAges.push(d3.select(this).property("id"))
    });

    return selectedIds;
  }

  getNestedData(data: Shooting[]) {
    console.log("#getNestedData")
    var nestedData = d3.nest<Shooting, number>()
                        .key(function(d) { return d.race; })
                        .key(function(d){
                          var ageGroup = null;
                          const age = parseInt(d.age);
                          if (age <= 20) {
                            ageGroup = "<20";
                          } else if (age > 20 && age <= 30 ){
                            ageGroup = "20-30";
                          } else if (age > 30 && age <= 40 ){
                            ageGroup = "30-40";
                          } else if (age > 40 && age <= 50 ){
                            ageGroup = "40-50";
                          } else if (age > 50 && age <= 60 ){
                            ageGroup = "50-60";
                          } else if (age > 60 ){
                            ageGroup = ">60"
                          }
                          return ageGroup;
                        })
                        .rollup((leaves) => {return leaves.length})
                        .entries(data);
    return nestedData;
  }
}

export class SelectedIds {
  selectedRaces: string[] = [];
  selectedAges: string[] = [];
}