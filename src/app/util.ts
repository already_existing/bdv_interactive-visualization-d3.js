export class Util {
    races: string[] = [];
    ageIds: string[] = ["<20", "20-30", "30-40", "40-50", "50-60", ">60"];
    ageNames: string[] = ["Under 20 Years", "20 to 30 Years", "30 to 40 Years", "40 to 50 Years", "50 to 60 Years", "60 Years and Over"];
    raceNamesMap: {[key: string]: string} = {"A":"Asian", "H":"Hispanic", "W":"White", "B":"Black", "N":"Native American", "O":"Other", "":"Unknown"};

    constructor() {
        for (const r in this.raceNamesMap) {
            this.races.push(r)
        }
    }
}  
