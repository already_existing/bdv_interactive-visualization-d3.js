// with public in constructor the property is automatically created
export class Shooting {
  constructor(public name: string, public date: string, public race: string, public age: string) { 
  }
}
