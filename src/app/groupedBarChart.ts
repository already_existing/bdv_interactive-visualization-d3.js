import * as d3 from "d3"
import { Shooting } from "./shooting";
import { Util } from "./util"
import { SelectedIds } from "./app";

export class GroupedBarChart {
  util = new Util();
  raceMap: string[];
  colorMap: {[key: string]: string} = {"<20":"#fee5d9", "20-30":"#fcbba1", "30-40":"#fc9272", "40-50":"#fb6a4a", "50-60":"#de2d26", ">60":"#a50f15"}
  x0Scale;
  x1Scale;
  yScale;
  xAxis;
  xAxisGroup;
  yAxis;
  yAxisGroup;
  updateData;
  
  constructor(public data: Shooting[]) {
    this.tooltip.append("rect")
      .attr("width", 30)
      .attr("height", 20)
      .attr("fill", "white")
      .style("opacity", 0.7)
    
    this.tooltip.append("text")
      .attr("x", 15)
      .attr("dy", "1.2em")
      .attr("font-family", "sans-serif")
      .attr("font-size", "12px")
      .attr("font-weight", "bold");
    
    // append y axis label
    this.svg.append("text")
    .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", `translate(${this.plotMargins.left / 2},${this.height / 2})rotate(-90)`)  // text is drawn off the screen top left, move down and out and rotate
    .attr("font-family", "sans-serif")
    .text("People shot");

    this.constructUniqueMaps();
/*     this.getNestedData();
 */    this.defineScalesAndAxes();
    this.renderGraph();
  }

  // plot area declarations
  readonly width = 800;
  readonly height = 350;
  readonly plotMargins = { top: 30, bottom: 30, left: 60, right: 30 };
  readonly plotWidth = this.width - this.plotMargins.left - this.plotMargins.right;
  readonly plotHeight = this.height - this.plotMargins.top - this.plotMargins.bottom; 
  
  // Creates sources <svg> element
  readonly svg = d3.select(".groupedBarChart").append("svg")
    .attr("width", this.width)
    .attr("height", this.height)
  
  readonly g = this.svg.append("g")
    .classed("plot", true)
    .attr("transform", `translate(${this.plotMargins.left},${this.plotMargins.top})`);

  readonly tooltip = this.svg.append("g")
    .classed("mousetooltip", true)
    .style("display", "none")

  constructUniqueMaps() {
    console.log("#constructUniqueMaps")
    this.raceMap = d3.map(this.data, d => {return d.race;}).keys()
    this.raceMap.splice(this.raceMap.indexOf(""), 1);
  }

  defineScalesAndAxes(){
    console.log("#defineScalesAndAxes")
    // Scales setup
    // x0 scale for each race
    this.x0Scale = d3.scaleBand()
                      .rangeRound([0, this.plotWidth])

    // x1 domain is all the age names, we limit the range to from 0 to a x0 band
    this.x1Scale = d3.scaleBand();

    // scale for race age count
    this.yScale = d3.scaleLinear()
                      .rangeRound([this.plotHeight, 0]);

    // Axis setup
    this.xAxis = d3.axisBottom(this.x0Scale);
    this.yAxis = d3.axisLeft(this.yScale);
  }

  renderGraph(){
    console.log("#renderGraph")
    this.x0Scale.domain(this.raceMap.map((race) => {return this.util.raceNamesMap[race]}));
    this.x1Scale.domain(this.util.ageNames).rangeRound([0, this.x0Scale.bandwidth()]);
    this.yScale.domain([0, 0]);

    this.g.append("g")
            .classed("x axis", true)
            .attr("transform", `translate(${0},${this.plotHeight})`)
            .call(this.xAxis)

    this.g.append("g")
            .classed("y axis", true)
            .call(this.yAxis)
  }

  update(data: any, selectedIds: SelectedIds) {
    console.log("#update")
    this.updateData = data.map((d) => {
        return {
          race: d.key,
          ages: selectedIds.selectedAges.map((id) => {
            var idx = this.util.ageIds.indexOf((id));
            return {
              id: id,
              name: this.util.ageNames[idx],
              count: d.values.reduce((acc, val) => { if (val.key === id) {
                                                      return acc + val.value;
                                                    } else {
                                                      return acc + 0;
                                                    }}, 0)
            }
          })
        }
      }
    );
    
    this.x0Scale.domain(selectedIds.selectedRaces.map((d) => {return this.util.raceNamesMap[d]}))
    this.x1Scale.domain(this.util.ageIds).rangeRound([0, this.x0Scale.bandwidth()])
    if (this.updateData != []) {
      this.yScale.domain([0, d3.max(this.updateData, (d: any) => { return d3.max(d.ages, (age:any) => {return age.count})})])
    }
    
    this.g.selectAll(".axis.x").call(this.xAxis)
    this.g.selectAll(".axis.y").call(this.yAxis)

    let race = this.g.selectAll(".race")
                    .data(this.updateData);

    let raceEnter = race.enter().append("g")
      .classed("race", true)
      .attr("transform", (d: any) => {return `translate(${this.x0Scale(this.util.raceNamesMap[d.race])}, 0)`;})
    
    race.exit().transition().remove()

    let age = race.merge(raceEnter)
      .attr("transform", (d: any) => {return `translate(${this.x0Scale(this.util.raceNamesMap[d.race])}, 0)`;})
      .selectAll("rect").data((d: any) => { return d.ages;});

    let ageEnter = age.enter().append("rect").attr("width", 0)
      .on("mouseover", () => { this.tooltip.style("display", null);})
      .on("mouseout", () => { this.tooltip.style("display", "none");})
      .on("mousemove", (d: any) => {
        const pos = d3.mouse(this.svg.node() as d3.ContainerElement);
        var xPosition = pos[0] - 15;
        var yPosition = pos[1] - 25;
        this.tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
        this.tooltip.select("text").text(d.name + ": " + d.count);
        const textElement: any = this.tooltip.select("text").node() as d3.ContainerElement;
        this.tooltip.select("rect").attr("width", textElement.getComputedTextLength() + 30)});

    age.merge(ageEnter)
      .attr("x", (d, idx) => {return this.x1Scale(this.util.ageIds[idx]);})
      .attr("y", (d: any) => {return this.yScale(d.count)})
      .attr("id", (d: any) => {return d.id})
      .style("fill", (d: any) => {return this.colorMap[d.id]})
      .text((d: any) => {return d.name})
      .transition()
      .attr("width", this.x1Scale.bandwidth())
      .attr("height", (d: any) => {return this.plotHeight - this.yScale(d.count);})

    age.exit().transition().attr("width", 0).remove();

    var legend;
    if (this.updateData.length != 0) {
      legend = this.g.selectAll(".legend").data(this.updateData[0].ages.map((age) => {return age.id}));
    } else {
      legend = this.g.selectAll(".legend").data([]);
    }

    let legendEnter = legend.enter().append("g");
    legend.merge(legendEnter)
        .classed("legend", true)
        .attr("transform", (d, i) => {return `translate(0, ${ i * 20 })`;})
    
    let legendColor = legend.merge(legendEnter).selectAll(".legend-color").data((d) => { return [d]; });
    let legendColorEnter = legendColor.enter().append("rect");
    
    legendColor.merge(legendColorEnter)
      .classed("legend-color", true)
      .attr("x", this.plotWidth - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", ((d: any) => {return this.colorMap[d]}));
  
    var legendText = legend.merge(legendEnter).selectAll(".legend-text").data((d) => { return [d]; });;
    let legendTextEnter = legendText.enter().append("text");
    
    legendText.merge(legendTextEnter)
      .classed("legend-text", true)
      .attr("x", this.plotWidth - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text((d: any) => { return this.util.ageNames[this.util.ageIds.indexOf(d)]; });
  
    legend.exit().remove();
  }
  
}
