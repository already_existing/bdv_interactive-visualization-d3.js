# Installation 

* Install Node (https://nodejs.org/en/)
* Make sure npm is available (check with `npm --version`). You possibly want to use the "Node.js command prompt".
* Navigate into the project directory and install all used dependencies: `npm install`
* Build and run the project using webpack: `npm start`
    * This transpiles all TypeScript files to JavaScript and bundles them to a single bundle.js, which is reference in the index.html.
    * Hot reloading is enabled.
* Visit http://localhost:8080 (or whichever port the webpack is telling you).